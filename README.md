# PRE theme for Keycloak SSO

# Documentation et sources

La documentation de Keycloak explique comment foncitonne les thèmes : [Themes - Server Developer Documentation](https://www.keycloak.org/docs/latest/server_development/#_themes).

Le thème PRÉ se base sur le thème Keycloak dont le code source se trouve ici : [Code source thème Keycloak](https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme).

# Tester en local

Pour tester le thème en local, exécuter la commande suivante en remplaçant `<chemin vers le repo>` par le chemin vers le dossier local de ce dépôt Git.

```
sudo docker run --rm --name=keycloak-dev \
    -p 127.0.0.1:9000:8080 \
    -e KEYCLOAK_ADMIN=admin \
    -e KEYCLOAK_ADMIN_PASSWORD=admin \
    --mount type=bind,source=<chemin vers repo>,target=/opt/keycloak/themes/pre \
    quay.io/keycloak/keycloak:latest \
    start-dev
```

Se rendre ensuite sur [http://127.0.0.1:9000] et cliquer sur _Administration Console_.
Se connecter avec les identifiants contenus dans la commande Docker précedente.
Ouvrir l'onglet _Realm settings_>_Themes_ et définir le thème **pre** dans les menus déroulants opur lesquels le thème est défini.

Pour tester le thème *login*, se déconnecter ou ouvrir le Keycloak local dans un onglet de navigation privée.

# Déployer

Se connecter sur le serveur et mettre à jour la copie locale du dépôt avec `sudo git pull`.
Puis redémarrer le service : `sudo systemctl restart keycloak`.

